// ==UserScript==
// @name        Jenkins
// @namespace   http://majun.com/jenkins
// @description Add GIT branch to job header and job list
// @version     0.3
// @updateUrl   https://bitbucket.org/majun/jenkins-user-js/raw/master/jenkins.user.js
// @include     http://jenkins.mgmt.lmc.cz:8080/*
// @include     http://jenkinsqa.mgmt.lmc.cz:8080/*
// @copyright   2014+, Martin Junger
// @grant       none
//
//   jQuery
// @require     http://code.jquery.com/jquery-1.10.1.min.js
//
// @run-at      document-end
// ==/UserScript==

/*
Changelog:
	0.3:
		- show multiple branches
	0.2:
		- add branch name to job list
	0.1:
		- add branch name to header
*/

var $jq = jQuery.noConflict();

function getBranch(url, callback) {
	var pattern = new RegExp("(/job/[^/]*/).*");
	url = url.replace(pattern, "$1") + "config.xml";

	if(url.indexOf('/job/') >= 0) {
		$jq.ajax({
			url: url,
			success: function(xml) {
				var data = (new XMLSerializer()).serializeToString(xml);
				var pos = data.indexOf('hudson.plugins.git.BranchSpec');
				var part = data.substr(pos, 300);
				var pattern = new RegExp("<name>(.*)</name>", "g");
				var branch = part.match(pattern);
				branch = branch.length ? branch.join('‖') : '-';

				callback(branch);
			}
		});
	}
}


// get branch for current job
if(document.URL.indexOf('/job/') >= 0) {
	getBranch(document.URL, function(branch) {
		$jq('h1').append(' <small>('+branch+')</small>');
	});
}

// get branch for list of jobs
if(document.URL.indexOf('/view/') >= 0) {
	$jq.each($jq.find('#projectstatus .model-link'), function(index, link) {
		var url = document.URL + $jq(link).attr('href');
		getBranch(url, function(branch) {
			$jq(link).after(' <small>('+branch+')</small>');
		});
	});
}

